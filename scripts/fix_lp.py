import asyncio

import db


async def run():
    print("setting up database")
    database = db.get_db(True)

    await database["users"].update_many(
        {},
        [{"$set": {
            "history.2021.1.lp":
                "$lp",
            "history.2021.1.matches":
                "$matches",
            "history.2021.1.normalized_wins":
                "$normalized_wins"
        }}]
    )
    # async for user in database["users"].find({}):
    #    database.update


if __name__ == "__main__":
    asyncio.run(run())
